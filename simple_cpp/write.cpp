#include<iostream>
#include<unistd.h> //for usleep
#include"GPIO.h"
using namespace exploringBB;
using namespace std;

int main(){
    GPIO outGPIO(60);

    outGPIO.setDirection(OUTPUT);

    outGPIO.streamOpen();

    for (int i=0; i<1000000; i++) {
        outGPIO.streamWrite(HIGH);
        outGPIO.streamWrite(LOW);
    }

    outGPIO.streamWrite(LOW);
    outGPIO.streamClose();

    return 0;
}
