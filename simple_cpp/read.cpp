#include<iostream>
#include<unistd.h> //for usleep
#include"GPIO.h"
using namespace exploringBB;
using namespace std;

int main(){
    GPIO outGPIO(60), inGPIO(48);

    outGPIO.setDirection(OUTPUT);
    inGPIO.setDirection(INPUT);

    outGPIO.streamOpen();

    while(true) {
        if ( inGPIO.getValue() == 1) {
            for (int i=0; i<1000000; i++) {
                outGPIO.streamWrite(HIGH);
                outGPIO.streamWrite(LOW);
            }

            break;
        }
    }

    outGPIO.streamWrite(LOW);
    outGPIO.streamClose();

	return 0;
}