# Linux Kernel Module (LKM)

### Building the LKM on BeagleBoard
```
apt-cache search linux-headers-$(uname -r)
sudo apt install linux-headers-$(uname -r)
```

### Compile and Run
Setup <br>
```
config-pin P9_12 gpio
config-pin P9_15 gpio
```

Compile <br>
```
make
```

Run <br>
```
sudo insmod <filename>.ko
```

Stop <br>
```
sudo rmmod <filename>
```


### For further information on GPIO kernel programming under Linux
- The GPIO Sysfs Interface for User Space: tiny.cc/beagle1602
- GPIO Interfaces (in Kernel Space): tiny.cc/beagle1603
- Linux Kernel Development, Robert Love, Addison-Wesley Professional; third edition (July 2, 2010) 978-0672329463