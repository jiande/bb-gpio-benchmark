import Adafruit_BBIO.GPIO as GPIO

gpio60 = "P9_12"
gpio48 = "P9_15"

GPIO.setup(gpio48, GPIO.IN)
GPIO.setup(gpio60, GPIO.OUT)

while(1):
    if GPIO.input(gpio48):
        GPIO.output(gpio60, GPIO.HIGH)
    else:
        GPIO.output(gpio60, GPIO.LOW)
