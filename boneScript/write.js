#!/usr/bin/env node

const b = require('bonescript');

const led = "P9_12";

b.pinMode(led, b.OUTPUT);

while(1) {
	b.digitalWrite(led, 1);
	b.digitalWrite(led, 0);
}
