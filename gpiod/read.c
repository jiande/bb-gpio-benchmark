#include <gpiod.h>
#include <stdio.h>
#include <unistd.h>

int main() 
{
	const char *chipname = "gpiochip1";
	struct gpiod_chip *chip;
	struct gpiod_line *gpio60;
	struct gpiod_line *gpio48;

	chip = gpiod_chip_open_by_name(chipname);

	gpio60 = gpiod_chip_get_line(chip, 28);
	gpio48 = gpiod_chip_get_line(chip, 16);

	gpiod_line_request_output(gpio60, "example", 0);
	gpiod_line_request_input(gpio48, "example");

	while(1)
	{
		val = gpiod_line_get_value(gpio48);
		if (val)
			gpiod_line_set_value(gpio60, 1);
		else
			gpiod_line_set_value(gpio60, 0);
	}

	gpiod_line_release(gpio60);
	gpiod_line_release(gpio48);
	
	return 0;
}
