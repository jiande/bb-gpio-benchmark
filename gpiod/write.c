#include <gpiod.h>
#include <stdio.h>
#include <unistd.h>

int main() 
{
	const char *chipname = "gpiochip1";
	struct gpiod_chip *chip;
	struct gpiod_line *gpio60;

	chip = gpiod_chip_open_by_name(chipname);

	gpio60 = gpiod_chip_get_line(chip, 28);
	
	gpiod_line_request_output(gpio60, "example", 0);

	while(1)
	{
		gpiod_line_set_value(gpio60, 1);
		gpiod_line_set_value(gpio60, 0);
	}
	
	return 0;
}
