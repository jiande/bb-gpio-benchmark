#!/bin/bash
# Short script to toggle a GPIO pin at the highest frequency

echo "out" > /sys/class/gpio/gpio60/direction
echo "in" > /sys/class/gpio/gpio48/direction

COUNTER=0

while [ 1 ]; do
    value=$(cat /sys/class/gpio/gpio48/value)
    if [[ $value -eq 1 ]] 
    then
        echo 1 > /sys/class/gpio/gpio60/value
    else
        echo 0 > /sys/class/gpio/gpio60/value
    fi
    let COUNTER=COUNTER+1
done
