Ref: https://elinux.org/EBC_Exercise_11b_gpio_via_mmap

P9_12  GPIO60  4804c000

GPIO_DATAOUT 0x13Ch

An easy way to read the contents of a memory location is with devmem2. First install it with:
sudo ./devmem2 0x4804c13c 