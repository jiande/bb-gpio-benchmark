#include <stdint.h>
#include <pru_cfg.h>
#include "resource_table_empty.h"
#include "prugpio.h"

#define P9_12	(0x1<<28)			// Bit position tied to P9_11 on Black

volatile register uint32_t __R30;
volatile register uint32_t __R31;

void main(void)
{
	uint32_t *gpio1 = (uint32_t *)GPIO1;
	
	while(1) {
		gpio1[GPIO_SETDATAOUT]   = P9_12;
		gpio1[GPIO_CLEARDATAOUT] = P9_12;
	}
}