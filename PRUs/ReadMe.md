Reference https://markayoder.github.io/PRUCookbook/03details/details.html#details_makefile <br>

## Read
Reads P9_25 and writes it to make  as fast as it can. <br>
```
export TARGET=read.pru0
make
```

## Write
Toggle the GPIO Pin P9_11 as fast as posible <br>
```
export TARGET=write.pru0 <br>
make
```